#!/usr/bin/make -f

vmajor=$(shell dpkg-parsechangelog | grep ^Version: | sed 's/[^0-9]*\([0-9]*\).*/\1/')
vminor=$(shell dpkg-parsechangelog | grep ^Version: | sed 's/[^0-9]*[0-9]*\.\([0-9]*\).*/\1/')
vpatchlevel=$(shell dpkg-parsechangelog | grep ^Version: | sed 's/[^0-9]*[0-9]*\.[0-9]*\.\([0-9]*\).*/\1/')
v=$(vmajor).$(vminor).$(vpatchlevel)
v1=$(vmajor).$(vminor).$(shell expr $(vpatchlevel) + 1)
dtmp=$(shell pwd)/debian/tmp

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/architecture.mk

%:
	dh $@

override_dh_auto_configure:
	LIBS="-Wl,-soname,libitk$(vmajor).so" \
	dh_auto_configure -- \
		       --includedir=/usr/include/itcl \
		       --enable-shared \
		       --enable-threads \
		       --with-itcl=/usr/lib/$(DEB_HOST_MULTIARCH) \
		       --with-tcl=/usr/lib/$(DEB_HOST_MULTIARCH) \
		       --with-tk=/usr/lib/$(DEB_HOST_MULTIARCH)

ovarride_dh_clean:
	[ ! -f Makefile ] || $(MAKE) clean
	[ ! -f Makefile ] || $(MAKE) distclean
	#
	rm -f itkConfig.sh pkgIndex.tcl

override_dh_auto_install:
	$(MAKE) prefix=$(dtmp)/usr \
		exec_prefix=$(dtmp)/usr \
		includedir=$(dtmp)/usr/include/itcl \
		install
	install -m 644 itkConfig.sh $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/itk*
	#
	# Fix up the itk libraries.
	mv $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/itk*/libitk$(v).so $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)
	mv $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/itk*/*.a $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)
	ln -s libitk$(v).so $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/libitk$(vmajor).so
	ln -s libitkstub$(v).a $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/libitkstub$(vmajor).a
	#  (itkConfig.sh doesn't get installed at all, so we install it here)
	sed -e "s:^\(ITK_\(BUILD_\)\?LIB_SPEC\)=.*$$:\1='-L/usr/lib/$(DEB_HOST_MULTIARCH) -litk$(vmajor)':" \
	    -e "s:^\(ITK_\(BUILD_\)\?STUB_LIB_SPEC\)=.*$$:\1='-L/usr/lib/$(DEB_HOST_MULTIARCH) -litkstub$(vmajor)':" \
	    -e "s:^\(ITK_SRC_DIR\)=.*$$:\1='/usr/include/itcl/itk-private':" \
	    -e "s:^\(ITK_LIB_FILE\)=.*$$:\1=libitk$(vmajor).so:" \
	    -e "s:^\(ITK_STUB_LIB_FILE\)=.*$$:\1=libitkstub$(vmajor).a:" \
	    -e "s:^\(ITCL_VERSION\)=.*$$:ITK_VERSION='$(v)':" \
	    -i $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/*/itkConfig.sh
	install -m 644 $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/*/itkConfig.sh $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)
	#
	# Move the Tcl library to /usr/lib/tcltk/$(DEB_HOST_MULTIARCH)
	install -d -m 755 $(dtmp)/usr/lib/tcltk/$(DEB_HOST_MULTIARCH)
	mv $(dtmp)/usr/lib/$(DEB_HOST_MULTIARCH)/itk$(v) $(dtmp)/usr/lib/tcltk/$(DEB_HOST_MULTIARCH)
	#
	# Fix up the pkgIndex.tcl
	sed -i -e's/\$$dir/\/usr lib $(DEB_HOST_MULTIARCH)/' \
	    $(dtmp)/usr/lib/tcltk/$(DEB_HOST_MULTIARCH)/itk$(v)/pkgIndex.tcl
	#
	# Fix up the include files.
	install -d $(dtmp)/usr/include/itcl/itk-private/generic
	install -m 644 generic/*.h $(dtmp)/usr/include/itcl/itk-private/generic
	#
	# Fix up the manpages.
	mv $(dtmp)/usr/share/man/mann $(dtmp)/usr/share/man/man3
	#
	cd $(dtmp)/usr/share/man/man3 && \
	  for i in *.n ; do \
	    sed -i -e's/^\.TH \([^ ]\+\) n /.TH \1 3itk /' $$i ; \
	    mv $$i `echo $$i | sed -e 's/\.n/\.3itk/'` ; \
          done

override_dh_auto_test:
	# Tests require X-Window, so, diable them

vv = 4.1.0

get-orig-source:
	wget -O itk4_$(vv).orig.tar.gz \
	     http://prdownloads.sourceforge.net/incrtcl/itk$(vv).tar.gz

.PHONY: override_dh_auto_configure override_dh_clean override_dh_auto_test \
	override_dh_auto_install get-orig-source
